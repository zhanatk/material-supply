var moment = require('moment');
const { Client, logger } = require('camunda-external-task-client-js');
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor');
const { Variables } = require("camunda-external-task-client-js");

// ################ Keycloak ##########################
let bearerTokenInterceptor = null;
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  });
}  

// ################ Camunda ##########################
const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 };
//const config = { baseUrl: 'http://localhost:8080/camunda/rest', use: logger };
const client = new Client(config);

var async = require('async');

const request = require('request')
//postgREST token:
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5hbHl0aWNzX3VzZXIifQ.Q8H_V6qh9w-mxHUY4_2IqmWNk_UdkbtacaOAsbP8peE'
const POSTGREST_TOKEN = process.env.POSTGREST_TOKEN?process.env.POSTGREST_TOKEN:token
const POSTGREST_URL = process.env.POSTGREST_URL
const KEYCLOAK_BASE_URL = process.env.KEYCLOAK_BASE_URL
const CAMUNDA_URL = process.env.CAMUNDA_URL

//---------------------------------------------------------------------------------------------------------------------------
// Материалы: Заявка на организацию поездки. Забор данных с базы
client.subscribe('material-supply-get-data', async function({ task, taskService }) {

  const emp = task.variables.getAllTyped().starter.value

  request.get({
    'url': POSTGREST_URL+'/k2_materials',
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        console.log('id = ' + JSON.parse(body)[0].id)
        const objects = JSON.parse(body);
        const variables = new Variables().setAllTyped({
          materialsDictionary: {
            value: objects,
            type: 'JSON',
          },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      } else {
        const variables = new Variables().setAllTyped({
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      }
    }
  })
});

//---------------------------------------------------------------------------------------------------------------------------
// Материалы: обновить поле резерв в базе
client.subscribe('update-reserve-1', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var materials = typedValues.materials.value
  update_reserve_1(task, taskService, 0, materials)
});

function update_reserve_1(task, taskService, index, array) {
  let element = array[index]

  request.get({
    'url': POSTGREST_URL+'/k2_materials?id=eq.'+element.id,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        console.log('id = ' + JSON.parse(body)[0].id)
        const object = JSON.parse(body)[0];
       
        request({
            method: 'PATCH',
            uri: POSTGREST_URL+'/k2_materials?id=eq.'+element.id,
            auth: {
              'bearer': POSTGREST_TOKEN
            },
            'content-type': 'application/json',
            body: JSON.stringify({
              reserve: object.reserve + element.count
            })
          }, function(error, response, body){
            if(error){
              console.log('error:=' + error);
            } else {
              console.log(body)
              index = index + 1
              if (index < array.length)
                update_reserve_1(task, taskService, index, array)
              else
                taskService.complete(task);
            }
        })
      }
    }
  })
}

//---------------------------------------------------------------------------------------------------------------------------
// Материалы: обновить поле резерв в базе обратно
client.subscribe('update-reserve-2', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var materials = typedValues.materials.value
  update_reserve_2(task, taskService, 0, materials)
});

function update_reserve_2(task, taskService, index, array) {
  let element = array[index]

  request.get({
    'url': POSTGREST_URL+'/k2_materials?id=eq.'+element.id,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        console.log('id = ' + JSON.parse(body)[0].id)
        const object = JSON.parse(body)[0];
        request({
            method: 'PATCH',
            uri: POSTGREST_URL+'/k2_materials?id=eq.'+element.id,
            auth: {
              'bearer': POSTGREST_TOKEN
            },
            'content-type': 'application/json',
            body: JSON.stringify({
              reserve: object.reserve - element.count
            })
          }, function(error, response, body){
            if(error){
              console.log('error:=' + error);
            } else {
              console.log(body)
              index = index + 1
              if (index < array.length)
                update_reserve_2(task, taskService, index, array)
              else
                taskService.complete(task);
            }
        })
      } else {
        index = index + 1
        if (index < array.length)
          update_reserve_2(task, taskService, index, array)
        else
          taskService.complete(task);
      }
    }
  })
}

//---------------------------------------------------------------------------------------------------------------------------
// Материалы: обновить поле резерв обратно и факт = факт - кол-во в базе
client.subscribe('update-fact-reserve', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var materials = typedValues.supplier_materials.value
  update_reserve_fact(task, taskService, 0, materials)
});

function update_reserve_fact(task, taskService, index, array) {
  let element = array[index]

  request.get({
    'url': POSTGREST_URL+'/k2_materials?id=eq.'+element.id,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        console.log('id = ' + JSON.parse(body)[0].id)
        const object = JSON.parse(body)[0];
        request({
            method: 'PATCH',
            uri: POSTGREST_URL+'/k2_materials?id=eq.'+element.id,
            auth: {
              'bearer': POSTGREST_TOKEN
            },
            'content-type': 'application/json',
            body: JSON.stringify({
              fact: object.fact - object.reserve,
              reserve: object.reserve - element.count
            })
          }, function(error, response, body){
            if(error){
              console.log('error:=' + error);
            } else {
              console.log(body)
              index = index + 1
              if (index < array.length)
                update_reserve_fact(task, taskService, index, array)
              else
                taskService.complete(task);
            }
        })
      } else {
        index = index + 1
        if (index < array.length)
          update_reserve_fact(task, taskService, index, array)
        else
          taskService.complete(task);
      }
    }
  })
}